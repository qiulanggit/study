# coding:utf-8

import unittest
from requests_api import RunMain
import HTMLTestRunnerCN
from requests_api import RunMain
from mock_api import mock_test
from mock import mock


class TestMethod(unittest.TestCase, RunMain):
    def test_01(self):
        url = "http://192.168.8.103:8888/login"
        data = {"name": "xiaoming", "pwd": "111"}
        method = "get"
        res = mock_test(self.run_main, data, url, method, data)  # 接口请求参数
        print res
        self.assertEqual(res["pwd"], "111", "测试失败")

    def test_02(self):
        url = "http://192.168.8.103:8888/ive"
        data = {"sex": "nan", "site": "hubei"}
        method = "get"
        res = mock_test(self.run_main, data, url, method, data)  # 接口请求参数
        print res
        self.assertEqual(res["sex"], "nan", "测试失败")


if __name__ == "__main__":
    unittest.main()
    # suite = unittest.TestSuite()
    # suite.addTest(TestMethod("test_01"))
    # suite.addTest(TestMethod("test_02"))
    #
    # filePath = 'D:\\web_report.html'
    # fp = file(filePath, 'wb', )
    # runner = HTMLTestRunnerCN.HTMLTestRunner(
    #     stream=fp,
    #     title=u'自动化测试报告',
    #     tester=u'邱烺'
    # )
    # runner.run(suite)
