# coding=utf-8

import requests
import json
import unittest

""" 接口自动化框架"""


class RunMain:
    def __init__(self, url, method, data=None):
        self.res = self.run_main(url, method, data)

    def send_get(self, url, data):
        res = requests.get(url=url, data=data)
        return res.json()

    def send_post(self, url, data):
        res = requests.post(url=url, data=data)
        return res.json()

    def run_main(self, url, method, data=None):
        res = None
        if method == "get":
            res = self.send_get(url,  data)
        else:
            res = self.send_post(url,  data)
        return res


if __name__ == "__main__":
    url = "http://192.168.8.103:8888/login"
    data = {"name": "xiaoming", "pwd": "111"}
    method = "get"
    run = RunMain(url, method, data)
    print run.res


