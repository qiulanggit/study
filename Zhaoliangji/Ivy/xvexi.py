# -*- coding: UTF-8 -*-
import unittest
from appium import webdriver
from time import sleep

desired_caps = {
    'platformName': 'Android',  # 平台名称
    'deviceName': 'SM-G9350',  # 设备名称
    'platformVersion': '5.1.1',  # 平台版本
    'appPackage': 'com.xianghe.ivy',  # 通过adb shell       cd  /data/dat
    'appActivity': 'com.xianghe.ivy.ui.module.welcom.XWLauncherActivity',  # aapt dump badging +包
    'unicodeKeyboaed': 'True',  # 默认字符串utf-8
    'resetKeyboaed': 'True'  # 隐藏键盘
}

driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)  # 连接appium 启动app
sleep(15)

# print(driver.get_window_size())
x = driver.get_window_size()['width']   #获取屏幕的高
y = driver.get_window_size()['height']  # 获取屏幕宽
def swipe_left():  #左滑
    driver.swipe(x*0.8 , y*0.5 , x*0.2 , y*0.5, 200)
def swipe_right(): #右滑
    driver.swipe(x*0.5,y*0.2 , x*0.5,y*0.8, 200)
def swipe_up():   #上滑
    driver.swipe(x*0.8,y*0.5 , x*0.1,y*0.5, 200)
def swipe_below():  #下滑
    driver.swipe(x*0.1,y*0.5 , x*0.8,y*0.5, 200)



class Auto_Test(unittest.TestCase):

    #  测试用例1，断言首页推荐
    def test_page (self):
        print "11"


    def op(self):
        print '22'


    #  测试用例2，断言登陆
    # def landing(self):
    #     try:
    #         driver.find_element_by_id("com.xianghe.ivy:id/btn_main_menu_mine").click()
    #         sleep(2)
    #         driver.find_element_by_id("com.xianghe.ivy:id/iv_activity_user_detail_avatar").click()
    #         sleep(2)
    #         driver.find_element_by_id("com.xianghe.ivy:id/et_activity_login_phone_number").send_keys('15629561737')
    #         sleep(4)
    #         driver.find_element_by_id("com.xianghe.ivy:id/et_activity_login_password").send_keys('123456')
    #         sleep(4)
    #         driver.find_element_by_id("com.xianghe.ivy:id/tv_activity_login_login").click()
    #         sleep(2)
    #         self.assertEqual(driver.find_elements_by_class_name('android.widget.TextView').text,u'i粉')
    #     except:
    #         print "用例2执行失败"


if __name__ == '__main__':
    unittest.main()


