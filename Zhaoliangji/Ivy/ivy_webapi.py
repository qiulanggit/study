# -*- coding: UTF-8 -*-
import requests
import unittest


class RequestTest(unittest.TestCase):
    """i微影后台管理接口测试"""

    def test_001_login(self):
        """登陆接口获取token"""
        url = 'http://precmsapi.i-weiying.com/admin/login'
        data = {
            'password': '123458',
            'username': 'qiulang',
            'sign': 'TY-8da23798c69b7bf706604f25c2c9549ae21593b6bdf4be64'
        }
        response = requests.post(url=url, data=data).json()
        code = response.status_code  # 响应码
        info = (response.json()["info"])
        self.assertEqual(info, 'ok', u"断言失败")
        print response.text.decode('unicode_escape')
        return (response.json()["data"]["token"])  # 获取token

    def test_002_media(self):
        """微影号搜索视频列表"""
        url = 'http://precmsapi.i-weiying.com/admin/media'
        data = {
            'ClientSource': '3',
            'ClientSystem': 'pc1.0',
            'Version': '3.1',
            'mobile_unique_code': 'web',
            'page': '1',
            'pagesize': '10',
            'ticket': '{}'.format(RequestTest.test_001_login(self)),
            'uid': '8273454',
            'sign': 'TY-8da23798c69b7bf706604f25c2c9549ae21593b6bdf4be64'
        }
        response = requests.post(url=url, data=data)
        code = response.status_code  # 响应码
        info = (response.json()["info"])  # 获取info
        print code, '微信号搜索视频列表', response.text.decode('unicode_escape')  # 反编码
        self.assertEqual(info, 'ok', u"断言失败")

    def test_003_media(self):
        """用户名搜索视频列表"""
        url = 'http://precmsapi.i-weiying.com/admin/media'
        data = {
            'ClientSource': '3',
            'ClientSystem': 'pc1.0',
            'Version': '3.1',
            'mobile_unique_code': 'web',
            'name': 'Lang',
            'page': '1',
            'pagesize': '10',
            'ticket': '{}'.format(RequestTest.test_001_login(self)),
            'sign': 'TY-8da23798c69b7bf706604f25c2c9549ae21593b6bdf4be64'
        }
        response = requests.post(url=url, data=data)
        code = response.status_code  # 响应码
        info = (response.json()["info"])  # 获取info
        print code, '用户名搜索视频列表', response.text.decode('unicode_escape')  # 反编码
        self.assertEqual(info, 'ok', u"断言失败")

    def test_004_media(self):
        """视频标题搜索视频"""
        url = 'http://precmsapi.i-weiying.com/admin/media'
        data = {
            'ClientSource': '3',
            'ClientSystem': 'pc1.0',
            'Version': '3.1',
            'mobile_unique_code': 'web',
            'title': 'ok',
            'page': '1',
            'pagesize': '10',
            'ticket': '{}'.format(RequestTest.test_001_login(self)),
            'sign': 'TY-8da23798c69b7bf706604f25c2c9549ae21593b6bdf4be64'
        }
        response = requests.post(url=url, data=data)
        code = response.status_code  # 响应码
        info = (response.json()["info"])  # 获取info
        print code, '视频标题搜索视频列表', response.text.decode('unicode_escape')  # 反向编码
        self.assertEqual(info, 'ok', u"断言失败")

    def test_005_media(self):
        """筛选公开发布视频"""
        url = 'http://precmsapi.i-weiying.com/admin/media'
        data = {
            'ClientSource': '3',
            'ClientSystem': 'pc1.0',
            'Version': '3.1',
            'is_private': '0',
            'mobile_unique_code': 'web',
            'page': '1',
            'pagesize': '10',
            'ticket': '{}'.format(RequestTest.test_001_login(self)),
            'sign': 'TY-8da23798c69b7bf706604f25c2c9549ae21593b6bdf4be64'
        }
        response = requests.post(url=url, data=data)
        code = response.status_code  # 响应码
        info = (response.json()["info"])  # 获取info
        print code, '筛选公开发布视频', response.text.decode('unicode_escape')  # 反编码
        self.assertEqual(info, 'ok', u"断言失败")

    def test_006_media(self):
        """筛选私密视频"""
        url = 'http://precmsapi.i-weiying.com/admin/media'
        data = {
            'ClientSource': '3',
            'ClientSystem': 'pc1.0',
            'Version': '3.1',
            'is_private': '1',
            'mobile_unique_code': 'web',
            'page': '1',
            'pagesize': '10',
            'ticket': '{}'.format(RequestTest.test_001_login(self)),
            'sign': 'TY-8da23798c69b7bf706604f25c2c9549ae21593b6bdf4be64'
        }
        response = requests.post(url=url, data=data)
        code = response.status_code  # 响应码
        info = (response.json()["info"])
        print code, '筛选私密视频', response.text.decode('unicode_escape')  # 反编码
        self.assertEqual(info, 'ok', u"断言失败")

    def test_007_media(self):
        """筛选亲友圈视频"""
        url = 'http://precmsapi.i-weiying.com/admin/media'
        data = {
            'ClientSource': '3',
            'ClientSystem': 'pc1.0',
            'Version': '3.1',
            'page': '1',
            'is_private': '2',
            'mobile_unique_code': 'web',
            'pagesize': '10',
            'ticket': '{}'.format(RequestTest.test_001_login(self)),
            'sign': 'TY-8da23798c69b7bf706604f25c2c9549ae21593b6bdf4be64'
        }
        response = requests.post(url=url, data=data)
        code = response.status_code  # 响应码
        info = (response.json()["info"])
        print code, '筛选亲友圈视频', response.text.decode('unicode_escape')  # 反编码
        self.assertEqual(info, 'ok', u"断言失败")


if __name__ == "__main__":
    unittest.main(verbosity=2)
