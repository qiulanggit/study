#  -*- coding: UTF-8 -*-
import pymysql
import requests
import random
import sys
import os
curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]
sys.path.append(rootPath)

""" 测试环境token获取 """


class YZM(object):
    @classmethod
    def times(self, t):
        x = 1
        token_list = []
        print(t)
        for i in range(int(t)):
            print (' === {} ==='.format(x))
            x += 1
            self.mobile_x = '131{}'.format(random.randint(10000000, 99999999))
            token_list.append(YZM.token_x())
            self.mobile_x = ''
        print ('all token:', token_list)
        return token_list

    @classmethod
    def yzma(self):
        # 发送验证码
        url = 'http://testuser.zhaoliangji.com/api/msgcode/send_sms'
        request_headers = {'Content-Type': 'application/x-www-form-urlencoded',
                           'Accept-Language': 'zh-Hans-CN;q=1',
                           'Accept-Encoding': 'gzip, deflate',
                           'Content-Length': '373'
                           }
        request_body = {'mobile': self.mobile_x,
                        'p': 'ios',
                        'sign': 'D903D14C33163C3B4CEDAF96052E5650',
                        'source': '4',
                        'timestr': '1572522560',
                        }
        response = requests.post(url=url, json=request_headers, data=request_body)
        #print '发送验证:', response.text.decode('unicode_escape')

    @classmethod
    def mysql(self):
        print ('mobile:', self.mobile_x)
        # 获取验证码
        YZM.yzma()
        conn = pymysql.connect(host='zljsz-testenvironmentdb.mysql.rds.aliyuncs.com',
                               user='code_test',
                               password='reVvtDiN2g3JaCxc',
                               database='panda_user',
                               use_unicode=True,
                               charset='utf8')

        cursor = conn.cursor()

        cursor.execute('select message from aci_sms_log order by id desc limit 1')
        info = cursor.fetchall()
        x = str(info)[-9:-5]
        print ('code:', x)
        cursor.close()
        conn.close()
        return x

    @classmethod
    def token_x(self):
        url = 'http://testuser.zhaoliangji.com/api/login/quick'
        request_headers = {'Content-Type': 'application/x-www-form-urlencoded'
                           }
        request_body = {'mobile': self.mobile_x,
                        'code': self.mysql()}

        response = requests.post(url=url, json=request_headers, data=request_body)
        token_x = response.json()['data']['token']
        print ('token:', token_x)
        print ('-' * 60)
        return token_x


if __name__ == '__main__':
    YZM.times(1)
