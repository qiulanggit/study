# coding:utf-8
import urllib
import re

def get_html(url):
    page = urllib.urlopen(url)  #打开网页
    html_code = page.read() #返回网页源码html
    return html_code

def get_image(html_code,reg):
    #reg = r'src="(.+?\.jpg)" width'  #正则表达式
    reg_img = re.compile(reg)  #编译一下，运行更快
    img_list = reg_img.findall(html_code)  #返回html_code中与reg_img匹配的字符串
    x = 0
    for img in img_list:
        urllib.urlretrieve(img,'E:\jgp\%s.jpg' % x)
        x+=1

get_html('http://699pic.com/nature.html')














