# coding=utf-8

#pythn 中常用的字符串处理函数


str = 'hello word!okokokokokok ppppppp,tttttt.okokokokokkkokok'
str1 = 'ok'
str2 = 'ko'
# #检测字符串是否包含特定字符，如果包含，则返回开始的索引；否则，返回-1。
# print str.find('wo')
#
# #检测字符串是否包含指定字符，如果包含，则返回开始的索引值；否则，提示错误。
# print str.index('wo')
#
# #返回str1在string中指定索引范围内[start, end)出现的次数。
# print str.count('ok',0,len(str))    #len()返回str字符串长度
#
# #将str中的str1替换成str2，如果指定count，则不超过count次;
# print (str.replace(str1,str2,2))
#
#
#maxSplit默认值为-1，表示根据定界符分割所有能分割的；
#str.split('分界符', maxSplit)
print str.split('o',6)


# with open('D:\\password','r') as f:  #读取指定路径下文件的数据
# y=f.read()                      #返回文件内容
# print "success出现次数：",y.count('success')   #统计
# print "fail出现次数：",y.count('fail')         #统计

