# coding=utf-8
import time

#
# a = 6
# i = 0
# print a * "*"
# while i < (a - 2):
#     print "*" + " " * (a - 2) + "*"
#     i = i + 1
# print a * "*"

# a = 5
# for i in range(a):
#     if i == 0:
#         print a * "*"
#     else:
#         if i == (a - 1):
#             print a * "*"
#         else:
#             print "*" + " " * (a - 2) + "*"

# 右对齐 乘法口诀
# for i in range(1, 10):
#     s = " "
#     for a in range(i, 10):
#         s += ("{}{}{}{}{:<3} ".format(i, "*", a, "=", i*a))
#     print "{:>75}".format(s)


# for i in range(1, 8, 2):
#     print " " * ((7 - i) / 2) + str(i * "*")
#     if i == 7:
#         for j in range(5, 0, -2):
#             print " " * ((7 - j) / 2) + str(j * "*


# 打印闪电
# for i in range(-3, 4):
#     if i < 0:
#         print (" "*(-i) + "*"*(4+i))
#     elif i > 0:
#         print (" "*3 + "*"*(4-i))
#     else:
#         print ("*"*7)


# 斐波那契数列
# start = time.clock()
# list = []
# for i in range(0, 200):
#     if i < 2:
#         list.append(i)
#     else:
#         list.append(list[-1]+list[-2])
# print list
# end = time.clock()
# print "run:", (end-start)


# 斐波那契数列
# a = 0
# b = 1
# while True:
#     c = a + b
#     if c < 100:
#         a = b
#         b = c
#         print c

# 猴子吃桃
# a = 1
# for i in range(9):
#     a = (a+1)*2
#     print a

# x = 3.4556
# print ("{}\n{}\n{}\n".format(int(x), float(x), bool(x)))

# a = [1, 3, 5, 66, 50, "a", 4]
# a.sort(key=str, reverse=True)
# print a

# 杨辉三角形
# triangle = [[1], [1, 1]]
# n = 6
# for i in range(2, n):             i = 2
#     pre = triangle[i - 1]       pre = triangle[1, 1]
#     cur = [1]
#     for j in range(0, i - 1):           j = 0
#         cur.append(pre[j], pre[j + 1])    pre[0],pre[1]   cur=[1,1,1]
#     cur.append(2)                        cur = [1,1,1,2]
#     triangle.append(cur)
#     print(triangle)
