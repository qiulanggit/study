#coding=utf-8

import urllib
import re
from time import sleep


def get_html(url):
    page = urllib.urlopen(url)  #打开网页
    html_code = page.read()     #返回网页源码html
    return html_code


def get_image(html_code,reg):
    reg_img = re.compile(reg)   #编译一下正则表达式，运行更快
    img_list = reg_img.findall(html_code)   #返回匹配的字符
    x = 0
    for img in img_list:
        urllib.urlretrieve(img, 'E:\jgp\%s.jpg' % x)   #下载到指定文件
        x += 1

if __name__=="__main__":

    get_image(get_html('http://tieba.baidu.com/p/5363455291?pn=1'),r'src="(.+?\.jpg)" size')


