# -*- coding: UTF-8 -*-
import unittest
from appium import webdriver
from time import sleep
import HTMLTestRunnerCN

desired_caps = {
    'platformName': 'Android',  # 平台名称
    'deviceName': 'SM-G9350',  # 设备名称
    'platformVersion': '5.1.1',  # 平台版本
    'appPackage': 'com.xianghe.ivy',  # 通过adb shell       cd  /data/dat
    'appActivity': 'com.xianghe.ivy.ui.module.welcom.XWLauncherActivity',  # aapt dump badging +包
    'unicodeKeyboaed': 'True',  # 默认字符串utf-8
    'resetKeyboaed': 'True'  # 隐藏键盘
}
driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)


class Slide(object):
    def __init__(self):
        self.x = self.driver.get_window_size()['width']
        self.y = self.driver.get_window_size()['height']

    def start(self):
        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)

    def swipe_left(self):  # 左滑
        self.driver.swipe(self.x * 0.8, self.y * 0.5, self.x * 0.2, self.y * 0.5, 200)

    def swipe_right(self):  # 右滑
        self.driver.swipe(self.x * 0.2, self.y * 0.5, self.x * 0.8, self.y * 0.5, 200)

    def swipe_up(self):  # 上滑
        self.driver.swipe(self.x * 0.5, self.y * 0.8, self.x * 0.5, self.y * 0.2, 200)

    def swipe_below(self):  # 下滑
        self.driver.swipe(self.x * 0.5, self.y * 0.2, self.x * 0.5, self.y * 0.8, 200)


slide = Slide()


class Shortcut(object, Slide):
    def home(self):
        self.driver.find_element_by_xpath(
            '//android.widget.FrameLayout/android.view.View/android.widget.ImageView').click()
        self.driver.find_element_by_id('com.xianghe.ivy:id/btn_main_menu_home').click()

    def mine(self):
        self.driver.find_element_by_xpath(
            '//android.widget.FrameLayout/android.view.View/android.widget.ImageView').click()
        self.driver.find_element_by_id('com.xianghe.ivy:id/btn_main_menu_mine').click()

    def camera(self):
        self.driver.find_element_by_xpath(
            '//android.widget.FrameLayout/android.view.View/android.widget.ImageView').click()
        self.driver.find_element_by_id('com.xianghe.ivy:id/btn_main_menu_camera').click()


shortcut = Shortcut


class Auto_Test(unittest.TestCase, Slide):
    # 微信登陆
    def test_001_Login(self):
        slide.swipe_left()

        # self.driver.find_element_by_id('com.xianghe.ivy:id/btn_main_menu_mine').click()
        # sleep(2)
        # self.driver.hide_keyboard()
        # self.driver.find_element_by_id('com.xianghe.ivy:id/ll_activity_login_login_weChat').click()
        # self.assertIsNotNone(self.driver.find_element_by_id('com.xianghe.ivy:id/btn_search'),'该页面无搜索元素')
        # sleep(2)



        # 手机号登陆
        # def test_002_Login(self):
        #     driver.find_element_by_xpath('//android.widget.FrameLayout/android.view.View/android.widget.ImageView').click()
        #     driver.find_element_by_id('com.xianghe.ivy:id/btn_main_menu_mine').click()
        #     driver.find_element_by_id('com.xianghe.ivy:id/iv_activity_user_setting').click()
        #     slide.swipe_up()
        #     driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_setting_quit').click()
        #     driver.find_element_by_id('com.xianghe.ivy:id/btn_main_menu_mine').click()
        #     driver.find_element_by_id('com.xianghe.ivy:id/et_activity_login_phone_number').send_keys('15629561737')
        #     driver.find_element_by_id('com.xianghe.ivy:id/et_activity_login_password').send_keys('123456')
        #     driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_login_login').click()
        #     self.assertIsNotNone(driver.find_element_by_id('com.xianghe.ivy:id/btn_search'),'该页面无搜索元素')
        #
        # #忘记密码
        # def test_003_Login(self):
        #     shortcut.mine()
        #     driver.find_element_by_id('com.xianghe.ivy:id/iv_activity_user_setting').click()
        #     slide.swipe_up()
        #     driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_setting_quit').click()
        #     shortcut.mine()
        #     driver.find_element_by_id('com.xianghe.ivy:id/et_activity_login_phone_number').send_keys('15629561737')
        #     driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_login_password_forget').click()                        #忘记密码
        #     driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_password_verify_code').click()                         #发送验证码
        #     driver.find_element_by_id('com.xianghe.ivy:id/et_activity_password_phone_verify_code').send_keys('123456')
        #     self.assertIsNotNone(driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_password_verify_code'),'无发送验证码元素')
        #     driver.find_element_by_id('com.xianghe.ivy:id/iv_activity_password_back').click()
        #     driver.find_element_by_id('com.xianghe.ivy:id/et_activity_login_password').send_keys('123456')
        #     driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_login_login').click()
        #     self.assertIsNotNone(self.driver.find_element_by_id('com.xianghe.ivy:id/btn_search'),'该页面无搜索元素')
        #
        # #验证码登陆+用户平台协议
        # def test_004_Login(self):
        #     shortcut.mine()
        #     driver.find_element_by_id('com.xianghe.ivy:id/iv_activity_user_setting').click()
        #     slide.swipe_up()
        #     driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_setting_quit').click()
        #     shortcut.mine()
        #     driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_login_user_protocol').click()
        #     self.assertIsNotNone(driver.find_element_by_id('com.xianghe.ivy:id/toolbar_activity_protocol'),'无用户协议元素')
        #     driver.find_elements_by_class_name('android.widget.ImageButton').click()
        #     driver.find_element_by_id('com.xianghe.ivy:id/et_activity_login_phone_number').send_keys('15629561737')
        #     driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_login_verify_login').click()
        #     driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_login_verify_code').click()
        #     self.assertIsNotNone(driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_login_verify_code'))
        #     driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_login_password_login').click()
        #     driver.find_element_by_id('com.xianghe.ivy:id/et_activity_password_phone_verify_code').send_keys('123456')
        #     self.assertIsNotNone(driver.find_element_by_id('com.xianghe.ivy:id/btn_search'),'该页面无搜索元素')












        # 平台用户阅读
        # try:
        #     driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_login_user_protocol').click()
        #     driver.find_element_by_class_name('android.widget.ImageButton').click()
        # except:
        #     driver.hide_keyboard()
        #     driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_login_user_protocol').click()
        #     driver.find_element_by_class_name('android.widget.ImageButton').click()
        # #忘记密码
        # driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_login_password_forget').click()                        #忘记密码
        # driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_password_verify_code').click()                         #发送验证码
        # driver.find_element_by_id('com.xianghe.ivy:id/et_activity_password_phone_verify_code').send_keys('123456')
        # driver.find_element_by_id('com.xianghe.ivy:id/et_activity_password_phone_verify_code').clear()
        # driver.find_element_by_id('com.xianghe.ivy:id/et_activity_password_password').send_keys('12345678910121')         #设置行密码
        # driver.find_element_by_id('com.xianghe.ivy:id/et_activity_password_password').clear()
        # driver.find_element_by_id('com.xianghe.ivy:id/iv_activity_password_back').click()                                 #back 返回到登陆界面
        # #验证码登陆
        # driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_login_verify_login').click()                           #验证码登陆
        # driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_login_verify_code').click()                            #发送验证码
        # driver.find_element_by_id('com.xianghe.ivy:id/et_activity_login_phone_verify_code').send_keys('123456789')       #输入验证吗
        # self.assertEqual(driver.find_element_by_id('com.xianghe.ivy:id/et_activity_login_phone_verify_code').text,'12345',u'验证码有误')
        # driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_login_password_login').click()                         #密码登陆
        # driver.find_element_by_id('com.xianghe.ivy:id/et_activity_login_password').send_keys('123456')
        # driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_login_login').click()
        # sleep(2)
        # shortcut.mine()
        # driver.find_element_by_id('com.xianghe.ivy:id/iv_activity_user_setting').click()
        # slide.swipe_up()
        # driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_setting_quit').click()
        # driver.get_screenshot_as_file(u'C:\\Android\\退出成功.png')
        # self.assertIsNone(driver.find_element_by_id('com.xianghe.ivy:id/tv_text'),u'该元素不存在')
        # #微信登陆
        # shortcut.mine()
        # driver.find_element_by_id('com.xianghe.ivy:id/ll_activity_login_login_weChat').clik()
        # shortcut.mine()
        # driver.find_element_by_id('com.xianghe.ivy:id/iv_activity_user_setting').click()     #系统设置
        # slide.swipe_up()
        # driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_setting_quit').click()








        # #2,左右滑动
        # def test_2_switch(self):
        #     home(self)
        #     sleep(1)
        #     a = 0
        #     while a < 4:
        #         swipe_left(self)
        #         a = a+1
        #     else:
        #         while a < 8:
        #             swipe_right(self)
        #             a = a+1
        #         else:
        #             print "ok"
        #     self.assertIsNotNone(driver.find_element_by_id('com.xianghe.ivy:id/iv_cover'))
        #

        #
        #
        #
        # #5,首页推荐 跳转
        # def test_5_recommend(self):
        #     driver.find_element_by_name(u'推荐').click()                                                                     # 切换首页推荐
        #     self.assertIsNotNone(driver.find_element_by_id('com.xianghe.ivy:id/layout_movie_info'),'fail')                    # 断言首页包含该元素
        #     driver.find_element_by_id('com.xianghe.ivy:id/btn_play').click()                                                  # 点击播放视频
        #     sleep(2)
        #     a = 0
        #     while a < 5:
        #         swipe_up(self)
        #         sleep(1)
        #         a = a+1
        #     else:
        #         while a < 10:
        #             swipe_below(self)
        #             sleep(1)
        #             a = a+1
        #         else:
        #             pass
        #     sleep(2)
        #     self.assertIsNotNone(driver.find_element_by_id('com.xianghe.ivy:id/layout_toolbar'),'fail')
        #     sleep(3)
        #     driver.swipe(x*0.2,y*0.9 , x*0.7,y*0.9, 500)                                                                          #快进视频
        #     sleep(3)
        #     driver.swipe(x*0.7,y*0.9 , x*0.2,y*0.9, 500)                                                                          #快退视频
        #     driver.swipe(x*0.5,y*0.5 , x*0.5,y*0.5, 100)                                                                        # 暂停播放
        #     sleep(2)
        #     driver.swipe(x*0.5,y*0.5 , x*0.5,y*0.5, 100)                                                                        # 播放
        #     driver.swipe(x*0.9,y*0.6 , x*0.9,y*0.2, 400)                                                                      # 音亮调大
        #     driver.swipe(x*0.9,y*0.2 , x*0.9,y*0.8, 400)                                                                      # 音量调小
        #     driver.find_element_by_id('com.xianghe.ivy:id/btn_comment').click()                                               # 评论按钮
        #     self.assertIsNotNone(driver.find_element_by_class_name('android.widget.LinearLayout'),'fail')
        #     driver.find_element_by_name(u'说点什么吧').click()                                                               # 点击输入框
        #     self.assertIsNotNone(driver.find_element_by_id('com.xianghe.ivy:id/btn_send'),'fail')                             # 断言包含 发送按钮
        #     sleep(1)
        #     driver.hide_keyboard()                                                                                            #隐藏硬盘
        #     sleep(1)
        #     driver.find_element_by_id('com.xianghe.ivy:id/btn_send').click()                                                   #点击发送
        #     driver.swipe(x*0.5,y*0.5 , x*0.5,y*0.5, 100)                                                                      #点击空白处
        #     driver.find_element_by_id('com.xianghe.ivy:id/btn_follow').click()                                                #收藏
        #     sleep(2)
        #     driver.find_element_by_id('com.xianghe.ivy:id/btn_follow').click()                                                #取消收藏
        #     driver.find_element_by_id('com.xianghe.ivy:id/btn_dianzan').click()                                               # 点赞
        #     driver.find_element_by_id('com.xianghe.ivy:id/btn_share').click()                                                 # 展开分享
        #     driver.find_element_by_id('com.xianghe.ivy:id/btn_share').click()                                                 # 收起分享
        #     driver.find_element_by_id('com.xianghe.ivy:id/iv_portrait').click()                                                 # 视频播放进入用户界面
        #     driver.find_element_by_id('com.xianghe.ivy:id/iv_activity_user_focus').click()                                      # 点击关注
        #     driver.find_element_by_id('com.xianghe.ivy:id/iv_activity_user_focus').click()                                      # 取消关注
        #     driver.find_element_by_id('com.xianghe.ivy:id/iv_activity_user_back').click()                                       # 返回全屏播放界面
        #     driver.find_element_by_id('com.xianghe.ivy:id/btn_toolbar_back').click()                                          #退出播放界面
        #     self.assertIsNotNone(driver.find_element_by_id('com.xianghe.ivy:id/iv_cover'))
        #
        #
        #
        #
        #
        # #6,首页亲友圈 跳转
        # def test_6_kith(self):
        #     driver.find_element_by_name(u'亲友圈').click()
        #
        #
        # #7,首页搜索
        # def test_7_search(self):
        #     driver.find_element_by_id('com.xianghe.ivy:id/btn_search').click()                                                  # 进入搜索界面
        #     driver.find_element_by_id('com.xianghe.ivy:id/et_activity_main_search').send_keys('Lang')                           # 搜索框输入Lang
        #     driver.find_element_by_id('com.xianghe.ivy:id/iv_activity_main_search_delete').click()                              # 清空输入框
        #     self.assertEqual(driver.find_element_by_id('com.xianghe.ivy:id/et_activity_main_search').text,u'搜索影片、用户')  # 断言 输入框包含 搜索影片、用户 元素
        #     driver.find_element_by_id('com.xianghe.ivy:id/et_activity_main_search').send_keys('Lang')                           # 搜索框输入 Lang
        #     driver.find_element_by_name(u'搜索').click()                                                                       # 点击 搜索 按钮
        #     sleep(1)
        #     driver.tap([(756,177),(1164,258)], 100)                                                                             # 用户
        #     sleep(1)
        #     driver.find_element_by_android_uiautomator('new UiSelector().text("影片")').click()                                 # 影片
        #     driver.find_element_by_android_uiautomator('new UiSelector().text("综合")').click()                                 # 综合
        #     driver.find_element_by_xpath('//android.support.v7.widget.RecyclerView/android.widget.RelativeLayout').click()      # 点击 搜索出来的用户
        #     driver.find_element_by_id('com.xianghe.ivy:id/iv_activity_user_myInfo_back').click()                                # 点击 返回搜索
        #     driver.find_element_by_xpath('//android.view.View/android.widget.ImageButton').click()                              # 点击 返回首页
        #     driver.find_element_by_id('com.xianghe.ivy:id/btn_search').click()                                                  # 进入搜索界面
        #     self.assertEqual(driver.find_element_by_id('com.xianghe.ivy:id/tv_item_main_search_history_keyWord').text,'Lang')   # 断言搜索记录 包含 Lang
        #     driver.find_element_by_id('com.xianghe.ivy:id/tv_item_main_search_history_keyWord').click()                         # 点击搜索记录 lang
        #     self.assertEqual(driver.find_element_by_id('com.xianghe.ivy:id/tv_item_main_search_user_nickname').text,'Lang')     # 断言 搜索内容包含 Lang
        #     driver.find_element_by_xpath('//android.view.View/android.widget.ImageButton').click()                              # 返回首页



        # 8,我的界面 跳转
        # def test_8_my(self):
        #     mine(self)


        # 9,系统设置 跳转
        # def test_9_settings(self):
        #     driver.find_element_by_id('com.xianghe.ivy:id/iv_activity_user_setting').click()
        #     driver.find_element_by_id('com.xianghe.ivy:id/stv_activity_setting_cache').click()                                  #清除缓存
        #     driver.find_element_by_id('com.xianghe.ivy:id/stv_activity_setting_protocol').click()                               #用户协议
        #     sleep(3)
        #     driver.find_element_by_class_name('android.widget.ImageButton').click()
        #     driver.find_element_by_id('com.xianghe.ivy:id/stv_activity_setting_aboutUs').click()                                #关于i微影
        #     sleep(2)
        #     driver.find_element_by_class_name('android.widget.ImageButton').click()
        #     driver.find_element_by_id('com.xianghe.ivy:id/cRightTextId').click()
        #     driver.find_element_by_id('com.xianghe.ivy:id/cRightTextId').click()
        #     swipe_up(self)
        #     sleep(2)
        #     driver.find_element_by_id('com.xianghe.ivy:id/stv_activity_setting_blacklist').click()                                     #黑名单
        #     driver.find_element_by_class_name('android.widget.ImageButton').click()
        #     driver.find_element_by_id('com.xianghe.ivy:id/stv_activity_setting_stockman').click()                                    # 股份
        #     sleep(2)
        #     driver.find_element_by_id('com.xianghe.ivy:id/iv_activity_stock_back').click()
        #     driver.find_element_by_id('com.xianghe.ivy:id/stv_activity_setting_vip').click()                                    #会员
        #     sleep(2)
        #     driver.find_element_by_id('com.xianghe.ivy:id/iv_activity_member_back').click()
        #     self.assertEqual(driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_setting_quit').text,u'退出登录')
        #     self.assertEqual(driver.find_element_by_id('com.xianghe.ivy:id/titleToolbar').text,u'系统设置')






        # 10,添加好友 跳转
        # def test_A_friend(self):
        #     # driver.find_element_by_class_name('android.widget.ImageButton').click()
        #     driver.find_element_by_id('com.xianghe.ivy:id/iv_activity_user_invite_friend').click()
        #     driver.find_element_by_id('com.xianghe.ivy:id/et_activity_invite_contact_search').send_keys('15629561737')
        #     driver.find_element_by_id('com.xianghe.ivy:id/textView_user_adapter_inviteFriend').click()                         #点击邀请
        #     driver.find_element_by_id('com.xianghe.ivy:id/iv_activity_invite_contact_delete').click()                          #清空输入框
        #     driver.swipe(x*0.9,y*0.7 , x*0.9,y*0.1)
        #     self.assertEqual(driver.find_element_by_id('com.xianghe.ivy:id/titleToolbar').text,u'邀请好友')




        # 添加好友 跳转
        # 方向按钮 跳转





        # 测试用例2，退出登陆
        # def test_B_logout(self):
        #      driver.find_element_by_id('com.xianghe.ivy:id/iv_activity_user_setting').click()
        #      swipe_up(self)
        #      driver.find_element_by_id('com.xianghe.ivy:id/tv_activity_setting_quit').click()
        #      self.assertEqual(driver.find_element_by_id('com.xianghe.ivy:id/tv_text').text,u'关注')
        #
        # #退出app
        #
        # def test_C_quit(self):
        #     sleep(3)
        #     driver.quit()


if __name__ == '__main__':
    unittest.main(verbosity=2)

'''输出html测试报告'''
# suit = unittest.TestSuite()
# suit.addTest(Auto_Test('test_1_Login'))
# suit.addTest(Auto_Test('test_2_ok'))
#
# filePath = 'C:\\ivy_test_report.html'
# fp = file(filePath,'wb',)
# runner = HTMLTestRunnerCN.HTMLTestRunner(
#     stream=fp,
#     title=u'i微影APP自动化测试报告',
#     tester=u'邱烺'
#     )
# runner.run(suit)
